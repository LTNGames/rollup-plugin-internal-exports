class Fluffykins {
  speak () {
    return this
  }
}

class _Doggykins {
  speak () {
    return this
  }
}

export const _InternalExport = 'Not exported to main module variable with iife'

export const _Private = []

export { Fluffykins, _Doggykins }
