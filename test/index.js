import test from 'ava'
import { rollup } from 'rollup'
import internalExports from './../dist/rollup-plugin-internal.js'
import path from 'path'

const inputOptions = {
  input: path.resolve('./test/helpers/entry.js'),
  plugins: [
    internalExports(
      { exports: ['_Doggykins', '_Private', '_InternalExport'] })
  ]
}

const outputOptions = {
  format: 'iife',
  name: 'ModuleName'
}

test('internal plugin is a function', t => {
  t.true((typeof internalExports === 'function'))
})

test('internal plugin returns object', t => {
  const result = internalExports()
  t.true(typeof result === 'object', 'must be an object')
  t.true(typeof result.transformBundle === 'function', 'must be a function')
})

test('remove multiple internal exports', async t => {
  const bundle = await rollup(inputOptions)
  const {code, map} = await bundle.generate(outputOptions)
  await t.true(!~code.indexOf('exports._Doggykins'), 'should no longer exist')
  await t.true(!~code.indexOf('exports._InternalExport'), 'should no longer exist')
  await t.true(!~code.indexOf('exports._Private'), 'should no longer exist')
  await t.false(!~code.indexOf('exports.Fluffykins'), 'must remain in exports')
})
