import MagicString from 'magic-string'
import filterText from './utils/filterText'

export default function internalExports (options = {}) {
  const _internals = options.exports

  const _isInternalExport = (name) => {
    return _internals.some(internal => internal === name)
  }

  return {
    name: 'internal-exports',

    transformBundle (code, id) {
      const magicCode = new MagicString(code)
      const pattern = /(exports.)([^\s]+).+/g
      const exportsToReplace = filterText(magicCode, pattern,
        (match) => _isInternalExport(match[2]))

      exportsToReplace.forEach(match => {
        const start = match.index
        const end = start + match[0].length
        magicCode.remove(start, end)
      })
      return { code: magicCode.toString(), map: magicCode.generateMap() }
    }
  }
}
