# rollup-plugin-internal-exports
Exclude certain exports from the final bundle when exporting your module with the iife format. Sometimes we need to export from a module in order to use it inside of another module but don't need it to expose it to the API.


**Take for example:**

```js
class Fluffykins {}

class Doggykins {}

export const _InternalExport = 'Only to be used within library'

export const _Private = ['Array', 'I dont', 'want exposed', 'to API']

export { Fluffykins, Doggykins }
```

Let's say we only want `Fluffykins` and `Doggykins` to be exported from our library but we want `_InternalExport` or `_Private` to not be exported in the final bundle.

**The final bundle output woud like like:**
```js
var MyModule = (function (exports) {
  
class Fluffykins {}

class Doggykins {}

const _InternalExport = 'Only to be used within library'
const _Private = []

exports.Fluffykins = Fluffykins
exports.Doggykins = Doggykins

})()
```

# Installation

First install it

```
npm install rollup-plugin-internal-exports --save-dev
```

# Usage

Import it into your rollup configuration

```js
import internal from 'rollup-plugin-internal-exports'

export default {
  input: 'src/index.js',
  output: {},
  plugins: [
    internal({
      exports: ['_Private', '_InternalExport']
      })
  ]
}
```
# Options

```js
{
  // The only option and it's just an array of exports you 
  // want to keep inernal if any of these strings are found
  // as an export they will be removed.
  exports: ['_privateExport', 'dontExportMe']
}
```
# License

MIT